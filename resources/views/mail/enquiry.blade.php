<?php ?>
<style>
.tbl-2 {
  border-collapse: collapse;
  border-spacing: 0;
  width: 50%;
  border: 1px solid #ddd;
}
.tbl-2 th, td {
  text-align: left;
  padding: 8px;
}
.tbl-2 th::after {
    content: ":";
    display: flex;
    margin-left: 95px;
    padding: 0px 14px;
    margin-top: -17px;
}
.tbl-2 tr:nth-child(even){background-color: #f2f2f2}
</style>
</head>
 <body>
  <p><h4>Dear Rsons Team,</h4></p>
    Below mentioned enquiry form has been filled by user.
  <br>
  <br>
  <table>
    <tr>
      <td><b>Name</b> </td>
      <td>: {{$data['name']??""}}</td>
    </tr>
    <tr>
      <td><b>Email</b> </td>
      <td>: {{$data['email']??""}}</td>
    </tr>
    <tr>
      <td><b>Mobile Number</b> </td>
      <td>: {{$data['number']??""}}</td>
    </tr>
    <tr>
      <td><b>Location</b> </td>
      <td>: {{$data['city']??""}}</td>
    </tr>
    <tr>
      <td><b>Message</b> </td>
      <td>: {{$data['desc']??""}}</td>
    </tr>
  </table>
<br>
<h4>Best regards</h4>