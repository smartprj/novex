@extends('layouts.admin')
@section('content')



    <div class="content-wrapper">
        <div class="page-header">
          <h3 class="page-title"> Edit Category </h3>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Category</li>
              <li class="breadcrumb-item active" aria-current="page">Edit Category</li>
            </ol>
          </nav>
        </div>

        @if ($errors->any())
            <div class="row">
                <div class="col-12">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif

        <div class="row">
          <div class="col-6 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    
                    <form action="{{ route('categories.update',$category->id) }}" method="POST">
                    @csrf
                    @method('PUT')

                      <div class="form-group">
                        <label for="exampleSelectcategory">Parent Category</label>
                        <select class="form-control" id="exampleSelectcategory" name="parent_id">
                            <option value="">Select Parent Category</option>
                            @foreach($catListing as $key=>$val)
                                <option value="{{$key}}" <?= ($category->parent_id == $key)?"Selected":""?>>{{$val}}</option>
                            @endforeach
                        </select>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputName1"><span style="color: red;">*</span> Name</label>
                        <input type="text" class="form-control" id="exampleInputName1" required="required" placeholder="Name" value="{{ $category->name }}" name="name">
                      </div>

                      <div class="form-group">
                        <label for="exampleSelectstatus"><span style="color: red;">*</span> Status</label>
                        <select class="form-control" id="exampleSelectstatus" required="required" name="status">
                          <option value="1" <?= ($category->status == 1)?"Selected":""?>>Active</option>
                          <option value="0" <?= ($category->status == 0)?"Selected":""?>>Inactive</option>
                        </select>
                      </div>
                      
                      <button type="submit" class="btn btn-gradient-primary mr-2">Submit</button>
                      <a href="{{route('categories.index')}}" class="btn btn-light">Cancel</a>
                    </form>
                </div>
            </div>
          </div>

        </div>
    </div>
    
@endsection