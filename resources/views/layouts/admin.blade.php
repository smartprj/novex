<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{ config('app.name', 'Rsons') }}</title>
    @include('inc.css')
    <link rel="shortcut icon" href="{{ asset('favicon-32x32.png') }}">
  </head>
  <body>
    <div class="container-scroller">
      @include('inc.header')
      <div class="container-fluid page-body-wrapper">
        @include('inc.sidebar')
        <div class="main-panel">
          @yield('content')
          @include('inc.footer')
        </div>
      </div>
    </div>
    @include('inc.js')
  </body>
</html>