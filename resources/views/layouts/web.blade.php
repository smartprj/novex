<!DOCTYPE html>
<html>
<head>
<title>{{ config('app.name', 'Rsons') }}</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Smart Shop Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<link rel="shortcut icon" href="{{ asset('favicon-32x32.png') }}">
@include('webinc.css')
</head>
<body>
    @include('webinc.header')
    @yield('content')
    @include('webinc.footer')
</body>
</html>