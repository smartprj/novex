<div class="coupons">
    <div class="row" style="    box-shadow: 0px -10px 20px 0px lightgrey;">
        @if(!empty($catHomeListing))
        <div class="text-center" style="height: 39.5rem; position: relative; top: 11rem;">
            @foreach ($catHomeListing as $cat)
            <div class="col-md-3 coupons-gd">
                <h3 style="color: #ab8d22;"><a href="{{route('listing',$cat['id'])}}" ><b class="btnOverlay">{{$cat['name']}}</b></a></h3>
            </div>
            @endforeach
            <div class="clearfix"> </div>
        </div>
        @endif
    </div>
</div>