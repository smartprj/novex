<div class="footer" style="background-color: black">
    <div class="container">
        <div class="col-md-3 footer-left">
            <h2><a href="{{ route('web') }}"><img src="{{ asset('web/images/rsonslogo.png') }}" style="width: 228px;height: 78px;" alt=" " /></a></h2>
            <p>Established in the year 2014, we, 
            RSONS PLUMBING SOLUTION PRIVATE LIMITED, 
            is leading Importers, manufacturer, 
            Wholesalers and suppliers of CP Bathroom Fittings, 
            SS fitting and Bath Accessories under the brand name NOVEX..
            Our State-of-the-art integrated production facilities located in India’ our head office in Chawri Bazaar, Delhi.
            <br/><small><a href="{{ route('aboutus') }}">read more...</a></small>
            </p>
        </div>
        <div class="col-md-9 footer-right">
            
            <div class="clearfix"></div>
            <div class="sign-grds">
                <div class="col-md-6 sign-gd">
                    <h4>Information</h4>
                    <ul>
                        <li><a href="{{ route('web') }}">Home</a></li>
                        <li><a href="{{ route('aboutus') }}">About</a></li>
                        <!-- <li><a href="womens.html">Women's Wear</a></li>
                        <li><a href="electronics.html">Electronics</a></li>
                        <li><a href="codes.html">Short Codes</a></li>
                        <li><a href="contact.html">Contact</a></li> -->
                    </ul>
                </div>
                
                <div class="col-md-6 sign-gd-two">
                    <h4>Store Information</h4>
                    <ul>
                        <li><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>Address : Rsons Plumbing Solution Private Limited,2800, Gali Chatta Sufi ,Peepal Mahadev ,Hauz Qazi <span>Delhi – 110006</span></li>
                        <li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i>Email : <small><a href="mailto:info@example.com">rsonsplumbing@gmail.com</a></small></li>
                        <li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>Phone : 9899242131</li>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="clearfix"></div>
        <!-- <p class="copy-right">&copy 2016 Smart Shop. All rights reserved | Design by <a href="http://w3layouts.com/">W3layouts</a></p> -->
    </div>
</div>