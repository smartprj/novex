<div class="footer">
    <div class="row text-center" style="margin-bottom:2rem;">
        <div class="col-md-3 text-center">
            <a href="{{ route('web') }}"><img src="{{ asset('web/images/rsonslogofooter.png') }}" style="width: 7rem;margin-left: 1rem;margin-top: 2rem;;" alt=" " /></a>
        </div>

        <div class="col-md-6 text-center">
            <div class="row" style="margin-top:2rem;">
                <div class="text-center">
                    <a href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook-square" style="margin-left: 15px;font-size:48px;color:#3b5998"></i></a>
                    <a href="https://twitter.com/rsonsplumbing" target="_blank"><i class="fa fa-twitter-square" style="margin-left: 15px;font-size:48px;color:#1DA1F2"></i></a>
                    <a href="https://rsons-plumbing-solution-private-limited.business.site/" target="_blank"><i class="fa fa-google-plus-square" style="margin-left: 15px;font-size:48px;color:#db4a39"></i></a>
                    <a href="https://www.instagram.com/rsonsplumbing21/" target="_blank"><i class="fa fa-instagram" style="margin-left: 15px;font-size:48px;color:#405DE6"></i></a>
                    <a href="https://www.linkedin.com/in/rsons-plumbing-solution-pvt-ltd-258243206/" target="_blank"><i class="fa fa-linkedin-square" style="margin-left: 15px;font-size:48px;color:#0e76a8"></i></a>
                    <a href="https://in.pinterest.com/rsonsplumbing" target="_blank"><i class="fa fa-pinterest-square" style="margin-left: 15px;font-size:48px;color:#BD081C"></i></a>
                    <a href="https://api.whatsapp.com/send?phone=919899242131&text=Interested in your product" target="_blank" style="transition: all 0.5s ease-in-out 0s;"><i class="fa fa-whatsapp" style="margin-left: 15px;font-size:48px;color:#4FCE5D"></i></a>

                </div>
            </div>
            <div class="row" style="margin-top:2rem;">
                <div class="" style="margin-top:0rem; margin-right: 1rem;">
                    <a href="{{ route('web') }}" style="margin-left: 20px;color: white;">Home</a>
                    <a href="{{ route('aboutus') }}" style="margin-left: 20px;color: white;">About Us</a>
                    <!-- <a href="" style="margin-left: 20px;color: white;">Our Products</a> -->
                    <a href="{{ asset('web/images/novex.pdf') }}" target="_blank" style="margin-left: 20px;color: white;">Catalogue</a>
                    <!-- <a href="{{ route('contactus') }}" style="margin-left: 20px;color: white;">Contact Us</a> -->
                    <!-- <a href="" style="margin-left: 20px;color: white;">Form</a> -->

<!--                     {{-- <a href="" style="margin-left: 20px;color: white;">Our Products</a> --}}
                    {{-- <a href="" style="margin-left: 20px;color: white;">Catalogue</a> --}}
 -->
<!--                     <a href="" style="margin-left: 20px;color: white;">Our Products</a>
                    <a href="" style="margin-left: 20px;color: white;">Catalogue</a> -->

                    <a href="{{ route('contactus') }}" style="margin-left: 20px;color: white;">Contact Us</a>
                    {{-- <a href="" style="margin-left: 20px;color: white;">Form</a> --}}
                </div>
                <div class="footerULine"></div>
            </div>
        </div>
        <div class="col-md-3" style="padding-top: 2rem;">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h4 class="text-white">Our Brands</h4>
                </div>
            </div>
            <div class="row" style="padding-top: 1.2rem;">
                <div class="col-md-6 text-center">
                    <img src="{{ asset('web/images/novexlogofooter.png') }}" style="width: 10rem;" alt=" " /><br>
                </div>
                <div class="col-md-6 text-center">
                    <img src="{{ asset('web/images/cosflologo.png') }}" style="width: 10rem;" alt=" " />
                </div>
            </div>
        </div>
    </div>
    <div class="row text-center">
        <div class="text-center">
            <h5 style="color:#fff">Copyright © 2020 - 21, RSONS, All Rights Reserved</h5>&nbsp<a href="http://www.techhint.in" target="_blank" class="text-white">Powered by www.techhint.in</a>
        </div> 
    </div>

</div>
<script type="text/javascript">
    
     $(function(){ 
     var navMain = $(".navbar-collapse"); // avoid dependency on #id
     // "a:not([data-toggle])" - to avoid issues caused
     // when you have dropdown inside navbar
     navMain.on("click", "a:not([data-toggle])", null, function () {
         navMain.collapse('show');
     });
 });

</script>