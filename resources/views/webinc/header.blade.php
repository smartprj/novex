<!-- <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"> -->
<!-- <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script> -->
<!-- <script src="//code.jquery.com/jquery-1.11.1.min.js"></script> -->
@php
$menuhtml='';
function generateMenu($categorylist=[]){
    $menuhtml='';
    if(!empty($categorylist)){
        $menuhtml.='<ul class="dropdown-menu">';
        foreach ($categorylist as $key => $value) {
            if(empty($value['child'])){
                $menuhtml.='
                <li class="menu-item dropdown">
                    <a href="'.route('listing',$key).'" >'.$value['parent'].'</a>
                </li>
                ';
            }else{
                $menuhtml.='
                <li class="menu-item dropdown dropdown-submenu">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">'.$value['parent'].'</a>
                    ';
                    if(!empty($value['child'])){
                        $menuhtml.=generateMenu($value['child'],$menuhtml);
                    }
                    $menuhtml.='</li>';
            }           
        }
        $menuhtml.='</ul>';
    }
    return $menuhtml;
}
$submenu = generateMenu($categorylist);
@endphp



<script type="text/javascript">
    (function ($) {
    $(document).ready(function () {
        $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function (event) {
            event.preventDefault();
            event.stopPropagation();
            $(this).parent().siblings().removeClass('open');
            $(this).parent().toggleClass('open');
        });
    });
})(jQuery);
</script>



<div class="header-bot row" style="height:8rem;">
    <div class="col-md-5 float-left paddingHeaderRow" style="background: #425249;">
        <div class="d-flex">
            <a href="{{ route('web') }}"><img src="{{ asset('web/images/rsonslogo.png') }}" style="width: 4.5rem; margin-left: 1.2rem;margin-top: 0.2em;background: #425249;"></a>
            &nbsp;<span class="text-white"><h2 style="margin:24px auto;">&nbsp;PLUMBING SOLUTION PVT LTD</h2></span>
        </div>
    </div>
    <div class="col-md-5 paddingHeaderRow" style="padding-top: 1.8rem;">
        <div class="ban-top">
            <div class="container">
                <div class="top_nav_left">
                    <nav class="navbar navbar-default">
                      <div class="container-fluid">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                          </button>
                        </div>
                        <div class="collapse navbar-collapse menu--shylock" id="bs-example-navbar-collapse-1">
                          <ul class="nav navbar-nav menu__list">
                            <li class="active menu__item {{ Route::is('web') ? "menu__item--current" : "" }}"><a class="menu__link" href="{{ route('web') }}">Home <span class="sr-only">(current)</span></a></li>
                            <li class=" menu__item {{ Request::is('*about*') ? "menu__item--current" : "" }}"><a class="menu__link" href="{{ route('aboutus') }}">About Us</a></li>
                            <li class="menu-item dropdown {{ Request::is('*product*') ? "menu__item--current" : "" }}">
                                <a href="javascript:void(0);" class="dropdown-toggle menu__link" data-toggle="dropdown">Product<b class="caret"></b></a>
                                @php
                                echo $submenu;
                                @endphp
                                {{-- <ul class="dropdown-menu">
                                    <li class="menu-item dropdown dropdown-submenu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Level 1</a>
                                        <ul class="dropdown-menu">
                                            <li class="menu-item ">
                                                <a href="#">Link 1</a>
                                            </li>
                                            <li class="menu-item dropdown dropdown-submenu">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Level 2</a>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <a href="#">Link 3</a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                </ul> --}}
                            </li>

                            <!-- <li class="active menu__item {{ Route::is('contactus') ? "menu__item--current" : "" }}"><a class="menu__link" href="{{ route('contactus') }}">Contact Us <span class="sr-only">(current)</span></a></li> -->

                            <li class="menu-item dropdown {{ Request::is('*contactus*') || Request::is('*enquiry*') ? "menu__item--current" : "" }}">
                                <a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown">Contacts <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li class="menu-item dropdown dropdown-submenu">
                                        <li>
                                            <a href="{{ route('contactus') }}">Contact Us</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('enquiry') }}">Enquiry Form</a>
                                        </li>
                                    </li>
                                </ul>
                            </li>


                          </ul>
                        </div>
                      </div>
                    </nav>  
                </div>
                <!-- </div> -->
            </div>
        </div>
        <div class="col-md-3 header-right footer-bottom">
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="col-md-2 float-right paddingHeaderRow novexlogoImg">
        <a href="{{ route('web') }}"><img class="" src="{{ asset('web/images/novexlogo.png') }}" style="width: 228px;height: 78px;background: #425249;"></a>
    </div>
</div>