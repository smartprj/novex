@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <div class="page-header">
          <h3 class="page-title">Product List</h3>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Product List</li>
            </ol>
            
          </nav>
        </div>

        @if ($message = Session::get('success'))
            <div class="row">
                <div class="col-12">
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                </div>
            </div>
        @endif

        <div class="row">
          <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <p class="card-description"> <a class="btn btn-success btn-sm float-sm-right" href="{{ route('products.create') }}"> Add Product</a></p>
                
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Image</th>
                      <th>Category</th>
                      <th>Sub Category</th>
                      <th>Name</th>
                      <th>Product Code</th>
                      <th>Home Listing</th>
                      <th>Home Branding</th>
                      <th>Status</th>
                      <th width="280px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($products as $product)
                    <tr>
                      <td>{{ ++$i }}</td>
                      <td class="py-1"><img src="{{ asset('images/products/'.$product->image) }}" alt="image" /></td>
                      <td>{{ $catListing[$product->category_id]??"" }}</td>
                      <td>{{ $catListing[$product->sub_category_id]??"" }}</td>
                      <td>{{ $product->name }}</td>
                      <td>{{ $product->product_code }}</td>
                      <td>
                        <div class="form-check">
                          <label class="form-check-label">
                          <input type="checkbox" name="home_listing" onchange="changeListing({{$product->id}})" class="form-check-input" <?= ($product->home_listing == 1)?"Checked":""?>> </label>
                        </div>
                      </td>
                      <td>
                        <div class="form-check">
                          <label class="form-check-label">
                          <input type="checkbox" name="home_branding" onchange="changeBranding({{$product->id}})" class="form-check-input" <?= ($product->home_branding == 1)?"Checked":""?>> </label>
                        </div>
                      </td>
                      <td>
                        @if($product->status == 1)
                            <label class="badge badge-success">Active</label>
                        @else
                            <label class="badge badge-danger">In-Active</label>
                        @endif
                      </td>
                      <td>
                        <form action="{{ route('products.destroy',$product->id) }}" method="POST">
                            <a class="btn btn-primary btn-xs" href="{{ route('products.edit',$product->id) }}">Edit</a>
                            <a class="btn btn-info btn-xs" href="{{ route('products.show',$product->id) }}">View</a>
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-xs">Delete</button>
                        </form>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                {!! $products->links() !!}
              </div>
            </div>
          </div>
        </div>
    </div>
    <script>
      function changeListing(id){
          $.ajax({
              url: '{{route("changelistingstatus")}}',
              data: { id: id},
              type: "GET",
              success: function (data) {
                  console.log('Status Updated Succesfully');
              }
         });
      }

      function changeBranding(id){
          $.ajax({
              url: '{{route("changebrandingstatus")}}',
              data: { id: id},
              type: "GET",
              success: function (data) {
                  console.log('Status Updated Succesfully');
              }
         });
      }
    </script>     
@endsection