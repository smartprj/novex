@extends('layouts.admin')
@section('content')

    <div class="content-wrapper">
        <div class="page-header">
          <h3 class="page-title"> View Product </h3>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Product</li>
              <li class="breadcrumb-item active" aria-current="page">View Product</li>
            </ol>
          </nav>
        </div>

        <div class="row">
          <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">

                  <div class="form-group">
                    <center><img src="{{ asset('images/products/'.$product->image) }}" alt="image" style="height: 250px;width:250px;" /></center>
                  </div>
                  <div class="form-group">
                    <table class="table table-hover">
                        <tr>
                          <th>Category</th>
                          <td>{{ $catListing[$product->category_id]??"" }}</td>
                        </tr>
                        <tr>
                          <th>Sub Category</th>
                          <td>{{ $catListing[$product->sub_category_id]??" - " }}</td>
                        </tr>
                        <tr>
                          <th>Name</th>
                          <td>{{ $product->name }}</td>
                        </tr>
                        <tr>
                          <th>Product Code</th>
                          <td>{{ $product->product_code }}</td>
                        </tr>
                        <tr>
                          <th>Description</th>
                          <td>{{ $product->description }}</td>
                        </tr>
                        <tr>
                          <th>Status</th>
                          <td>
                             @if($product->status == 1)
                              <label class="badge badge-success">Active</label>
                            @else
                                <label class="badge badge-danger">In-Active</label>
                            @endif
                          </td>
                        </tr>
                        <tr>
                          <th colspan="2"><a href="{{route('products.index')}}" class="btn btn-primary">Product Listing</a></th>
                        </tr>
                    </table>
                  </div>

                </div>
            </div>
          </div>

        </div>
    </div>

@endsection