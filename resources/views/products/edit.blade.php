@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <div class="page-header">
          <h3 class="page-title"> Edit Product </h3>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Product</li>
              <li class="breadcrumb-item active" aria-current="page">Edit Product</li>
            </ol>
          </nav>
        </div>

        @if ($errors->any())
            <div class="row">
                <div class="col-12">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif

        <div class="row">
          <div class="col-6 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    
                    <form action="{{ route('products.update',$product->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')

                      <div class="form-group">
                        <label for="exampleSelectcategory"><span style="color: red;">*</span> Category</label>
                        <select class="form-control" id="exampleSelectcategory" name="category_id" required="required" onchange="getSubCat(this.value)">
                            <option value="">Select Category</option>
                            @foreach($catListing as $key=>$val)
                                <option value="{{$key}}" <?= ($product->category_id == $key)?"Selected":""?>>{{$val}}</option>
                            @endforeach
                        </select>
                      </div>

                      <div class="form-group">
                        <label for="exampleSelectcategory">Sub Category</label>
                        <div class="sub-cat-input">
                          <select class="form-control" id="exampleSelectcategory" name="sub_category_id">
                              <option value="">Select Sub Category</option>
                              @foreach($subCatListing as $skey=>$sval)
                                  <option value="{{$skey}}" <?= ($product->sub_category_id == $skey)?"Selected":""?>>{{$sval}}</option>
                              @endforeach
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputName1"><span style="color: red;">*</span> Name </label>
                        <input type="text" value="{{ $product->name }}" class="form-control" id="exampleInputName1" required="required" placeholder="Name" name="name">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputName1"><span style="color: red;">*</span> Product Code </label>
                        <input type="text" value="{{ $product->product_code }}" class="form-control" id="exampleInputName1" required="required" placeholder="Product Code" name="product_code">
                      </div>

                      <div class="form-group">
                        <label for="exampleTextarea1">Description</label>
                        <textarea class="form-control" id="exampleTextarea1" rows="4" name="description">{{ $product->description }}</textarea>
                      </div>

                      <div class="form-group">
                        <label> Upload Image</label>
                        <input type="file" name="product_image" accept="image/*" class="file-upload-default">
                        <div class="input-group col-xs-12">
                          <input type="text" name="file" accept="image/*" class="form-control file-upload-info" disabled placeholder="Upload Image">
                          <span class="input-group-append">
                            <button name="sasa" class="file-upload-browse btn btn-gradient-primary" type="button">Upload</button>
                          </span>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="exampleSelectstatus"><span style="color: red;">*</span> Status </label>
                        <select class="form-control" id="exampleSelectstatus" required="required" name="status">
                          <option value="1" <?= ($product->status == 1)?"Selected":""?>>Active</option>
                          <option value="0" <?= ($product->status == 0)?"Selected":""?>>Inactive</option>
                        </select>
                      </div>
                      
                      <button type="submit" class="btn btn-gradient-primary mr-2">Submit</button>
                      <a href="{{route('products.index')}}" class="btn btn-light">Cancel</a>
                    </form>
                </div>
            </div>
          </div>

        </div>
    </div>
    <script>
      function getSubCat(id){
          $.ajax({
              url: '{{route("subcat")}}',
              data: { id: id},
              type: "GET",
              success: function (data) {
                  if(data != ""){
                      $('.sub-cat-input').html(data);
                  }
              }
         });
      }
    </script> 
    
@endsection