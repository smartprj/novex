@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <div class="page-header">
          <h3 class="page-title">Enquiry Form List</h3>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Enquiry Form List</li>
            </ol>
            
          </nav>
        </div>

        @if ($message = Session::get('success'))
            <div class="row">
                <div class="col-12">
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                </div>
            </div>
        @endif

        <div class="row">
          <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <p class="card-description"></p>
                
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Mobile Number</th>
                      <th>City</th>
                      <th>Message</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($enquiries as $enquery)
                    <tr>
                      <td>{{ ++$i }}</td>
                      <td>{{ $enquery->name }}</td>
                      <td>{{ $enquery->email }}</td>
                      <td>{{ $enquery->number }}</td>
                      <td>{{ $enquery->city }}</td>
                      <td>{{ $enquery->message }}</td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                {!! $enquiries->links() !!}
              </div>
            </div>
          </div>
        </div>
    </div>      
@endsection