@extends('layouts.web')
@section('content')
    <!-- product-nav -->
    <div class="product-easy">
        <div class="container">
            
            
            <div class="sap_tabs">
                <div id="horizontalTab" style="display: block; width: 100%; margin: 0px;">
                    <ul class="resp-tabs-list">
                        <li class="resp-tab-item" aria-controls="tab_item-0" role="tab"><span>{{$getCatDetail['name']??"Product Listing"}}</span></li> 
                    </ul>                    
                    <div class="resp-tabs-container">
                        <div class="tab-1 resp-tab-content" aria-labelledby="tab_item-0">
                            @if(!empty($products))
                                @foreach ($products as $product)
                                    <div class="col-md-4 product-men" onclick="location.href='{{route('detail',$product['product_code'])}}';" style="cursor: pointer;">
                                        <div class="men-pro-item simpleCart_shelfItem">
                                            <div class="men-thumb-item">
                                                <img src="{{ asset('images/products/'.$product['image']) }}" style="height: 360px;" alt="" class="pro-image-front">
                                                <img src="{{ asset('images/products/'.$product['image']) }}" style="height: 360px;" alt="" class="pro-image-back">
                                                <div class="men-cart-pro">
                                                    <div class="inner-men-cart-pro">
                                                        <a href="{{route('detail',$product['product_code'])}}" class="link-product-add-cart">{{$product['name'] ?? ""}}  ({{ $product['product_code']??""}})</a>
                                                    </div>
                                                </div>  
                                            </div>
                                            
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <center>No Products Found</center>
                            @endif
                            <div class="clearfix"></div>
                        </div>
                    </div>  
                </div>
            </div>
        </div>
    </div>
    <!-- //product-nav -->
@endsection