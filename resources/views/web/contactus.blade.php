@extends('layouts.web')
@section('content')
<div class="page-head">
    <div class="container">
        <h3 class="pageHeadTitle">Contact Us<br><b><span class="pageSubHeadTitle">RSONS PLUMBING SOLUTION PRIVATE LIMITED</span></b></h3>
    </div>
</div>

<div class="contact">
    <div class="container">
        <div class="contact-grids">
            <div class="col-md-4 contact-grid text-center animated wow slideInLeft" data-wow-delay=".5s">
                <div class="contact-grid1">
                    <i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>
                    <h4>Address</h4>
                    <p>Rsons Plumbing Solution Private Limited,<br>2800, Gali Chatta Sufi ,Peepal Mahadev ,
                        <span>Hauz Qazi Delhi – 110006</span></p>
                </div>
            </div>
            <div class="col-md-4 contact-grid text-center animated wow slideInUp" data-wow-delay=".5s">
                <div class="contact-grid2">
                    <i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>
                    <h4>Call Us</h4>
                    <p><span>9899242131</span></p>
                </div>
            </div>
            <div class="col-md-4 contact-grid text-center animated wow slideInRight" data-wow-delay=".5s">
                <div class="contact-grid3">
                    <i class="glyphicon glyphicon-envelope" aria-hidden="true"></i>
                    <h4>Email</h4>
                    <p><span><a href="mailto:rsonsplumbing@gmail.com">rsonsplumbing@gmail.com</a></span></p>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
        <div class="map wow fadeInDown animated" data-wow-delay=".5s">
            <h3 class="tittle">View On Map</h3>
            <iframe src="https://www.google.com/maps?q=28.6504,77.2262&hl=es;z=14&amp;output=embed" frameborder="0" style="border:0"></iframe>
        </div>
        <h3 class="tittle">Contact Form</h3>
        <form action="{{ route('storecontactus') }}" method="POST">
        @csrf
            <div class="contact-form2">
                <input type="text" name="name" placeholder="Enter Name" required="required">
                <input type="email" name="email" placeholder="Enter Email" required="required">
                <input type="number" name="number" placeholder="Enter Mobile Number">
                <input type="text" name="city" placeholder="Enter Your City Name" style="margin-left:1.5%;">
                <textarea type="text" name="message" required="required" placeholder="Enter Message"></textarea>
                <input type="submit" value="Submit" >
            </div>
        </form>
    </div>
</div>
@include('webinc.catfooter')
@endsection