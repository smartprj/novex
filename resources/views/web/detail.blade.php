@extends('layouts.web')
@section('content')
    <!-- product-nav -->
    <div class="product-easy">
        <div class="container">
            
            
            <div class="sap_tabs">
                <div id="horizontalTab" style="display: block; width: 100%; margin: 0px;">
                    <ul class="resp-tabs-list">
                        <li class="resp-tab-item" aria-controls="tab_item-0" role="tab"><span>{{$product['name']??"Product Detail"}}</span></li> 
                    </ul>                    
                    <div class="resp-tabs-container">
                        <div class="tab-1 resp-tab-content" aria-labelledby="tab_item-0">
                            @if(!empty($product))
                                <div class="col-md-12">
                                    <div class="col-sm-4 content-img-left text-center">
                                        <div class="content-grid-effect slow-zoom vertical">
                                            <div class="img-box"><img src="{{ asset('images/products/'.$product['image']) }}" style="height: 360px;" alt="image" class="img-responsive zoom-img"></div>
                                                <div class="info-box">
                                                    <div class="info-content simpleCart_shelfItem">
                                                        <h4>{{$product['name']}}</h4>
                                                        <span class="separator"></span>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-8 content-img-right" style="padding: 40px 20px 0px; position: relative;">
                                        <h3><span style="font-size: 20px">Details</span></h3>
                                        <a href="{{ route('enquiry') }}" class="gotoInquiry">
                                            <img src="{{ asset('web/images/inquiry.png') }}" style="width: 1.2rem; margin: 0px 0px 4px;"> Enquiry
                                        </a>
                                        <table style="width:100%" class="table">
                                            @if(!empty($product['category_id']))
                                                <tr>
                                                    <th>Category</th>
                                                    <td>{{$catListing[$product['category_id']]??""}}</td>
                                                </tr>
                                            @endif
                                            @if(!empty($product['sub_category_id']))
                                              <tr>
                                                <th>Sub Category</th>
                                                <td>{{$catListing[$product['sub_category_id']]??""}}</td>
                                              </tr>
                                            @endif
                                            @if(!empty($product['name']))
                                              <tr>
                                                <th>Product Name</th>
                                                <td>{{$product['name']??""}}</td>
                                              </tr>
                                            @endif
                                            @if(!empty($product['product_code']))
                                              <tr>
                                                <th>Product Code</th>
                                                <td>{{$product['product_code']??""}}</td>
                                              </tr>
                                            @endif
                                            @if(!empty($product['description']))
                                              <tr>
                                                <th>Description</th>
                                                <td>{{$product['description']??""}}</td>
                                              </tr>
                                            @endif
                                        </table>
                                    </div>
                                    
                                    <div class="clearfix"></div>
                                </div>
                                
                            @else
                                <center>No Details Found</center>
                            @endif
                            <div class="clearfix"></div>
                        </div>
                    </div>  
                </div>
            </div>
        </div>
    </div>
    <!-- //product-nav -->
@endsection