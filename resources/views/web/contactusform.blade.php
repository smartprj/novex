@extends('layouts.web')
@section('content')
<div class="page-head">
    <div class="container">
        <h3>Enquiry Form<br><b><span style="font-size: 20px">RSONS PLUMBING SOLUTION PRIVATE LIMITED</span></b></h3>
    </div>
</div>

<div class="contact">
    <div class="container">
        <h3 class="tittle">Enquiry Form</h3>
        <form action="{{ route('storecontactus') }}" method="POST">
        @csrf
            <div class="contact-form2">
                <input type="text" name="name" placeholder="Enter Name" required="required">
                <input type="email" name="email" placeholder="Enter Email" required="required">
                <input type="number" name="number" placeholder="Enter Mobile Number">
                <input type="text" name="city" placeholder="Enter Your City Name" style="margin-left:1.5%;">
                <textarea type="text" name="message" required="required" placeholder="Enter Message"></textarea>
                <input type="submit" value="Submit" >
            </div>
        </form>
    </div>
</div>
@include('webinc.catfooter')
@endsection