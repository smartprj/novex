@extends('layouts.web')
@section('content')
  <!-- banner -->
<!--     <div class="banner-grid">
        <div id="visual">
                <div class="slide-visual">
                    <ul class="slide-group">
                        <li><img class="img-responsive" src="{{ asset('web/images/banner1.jpg') }}" alt="Dummy Image" /></li>
                        <li><img class="img-responsive" src="{{ asset('web/images/banner2.jpg') }}" alt="Dummy Image" /></li>
                        <li><img class="img-responsive" src="{{ asset('web/images/banner3.jpg') }}" alt="Dummy Image" /></li>
                    </ul>
                    
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
    </div> -->
    <!-- //banner -->

<!-- Slider Begins -->
                <div class="slider">
                    <div class="slide show">
                        <img src="{{ asset('web/images/banner1.jpg') }}" class="img-responsive" />
                        <div class="overlay"></div>
                        <div class="content-top">
                                <div class="captionCompany">RSONS</div>
                                <div class="caption">INSPIRATIN FOR YOUR BATH</div>
                                <div class="subcaption">providing freedom of moment</div>
                                <div class="overlayButtons">
                                    <a href="#collection" class="btnOverlay mr-4">Collections</a>
                                    <a href="#product" class="btnOverlay ml-4">Our Products</a>
                                </div>
                        </div>
                    </div>
                     <div class="slide">
                        <img src="{{ asset('web/images/banner2mainpage.jpg') }}" class="img-responsive" />
                        <div class="overlay"></div>
                        <div class="content-top">
                            <div class="captionCompany">RSONS</div>
                            <div class="caption">NEW MODERN COLLECTION</div>
                            <div class="subcaption">for your bathroom</div>
                            <div class="overlayButtons">
                                <a href="#collection" class="btnOverlay mr-4">Collections</a>
                                <a href="#product" class="btnOverlay ml-4">Our Products</a>
                            </div>
                        </div>
                    </div>
                    <div class="slide">
                        <img src="{{ asset('web/images/banner3.jpg') }}" class="img-responsive" />
                        <div class="overlay"></div>
                        <div class="content-top">
                            <div class="captionCompany">RSONS</div>
                            <div class="caption">HIGH QUALITY PLUMBING SOLUTIONS</div>
                            <div class="subcaption">for your home renovation needs</div>
                            <div class="overlayButtons">
                                <a href="#collection" class="btnOverlay mr-4">Collections</a>
                                <a href="#product" class="btnOverlay ml-4">Our Products</a>
                            </div>         
                        </div>               
                    </div>
       
                <a class="prevmain" style="visibility: visible;">&#10094;</a>
                <a class="nextmain" style="visibility: visible;">&#10095;</a>
                </div>
                <script type="text/javascript">
                    const images = document.querySelectorAll(".slide"),
                    next = document.querySelector(".nextmain"),
                    prev = document.querySelector(".prevmain");

                    let current = 0;

                    function changeImage() {
                        images.forEach(img => {
                            img.classList.remove("show");
                            img.style.display = "none";
                        });

                        images[current].classList.add("show");
                        images[current].style.display = "block";
                    }

                // Calling first time
                changeImage();

                next.addEventListener("click", function() {
                    current++;

                    if (current > images.length - 1) {
                        current = 0;
                    } else if (current < 0) {
                        current = images.length - 1;
                    }

                    changeImage();
                });
                prev.addEventListener("click", function() {
                    current--;

                    if (current > images.length - 1) {
                        current = 0;
                    } else if (current < 0) {
                        current = images.length - 1;
                    }

                    changeImage();
                });

                // Auto change in 5 seconds

                setInterval(() => {
                    next.click();
                }, 5000);

                </script>
<!-- Slider Ends -->    

    <!-- content -->
    <div class="home_intro">
        <div class="uLine text-center" align="center" style="display: grid;">
            <h3><span>Rsons Plumbing Solution Private Limited</span></h3>
            <p><b>Rsons Plumbing Solution Pvt Ltd</b> is a leading manufacturer Wholesaler and supplier of
            Bathroom Fittings and Bath Accessories in stainless steel, Brass and ABS under the brand name
            <b>NOVEX</b> and <b>COSFLO</b>. Is located in India. Our Products are supported by a professional
            designing team.</p>
            <br><br>
            <p>
                <b>“We Believes in Producing Superior Quality Bath Fittings”</b>
            </p> 
            <p>
                <a href="{{ route('aboutus') }}" class="readMore">
                    Read More 
                    <img src="{{ asset('web/images/readmore.png') }}" style="width: 1.2rem; margin: 0px 0px 4px;">
                </a>
            </p>
        </div>
    </div>
    <!-- //content -->
    <!-- content-bottom -->
    @if(!empty($brandListing))
    <div class="container row" style="margin: 0 auto;"  id="collection">
        @foreach ($brandListing as $brand)

        <div class="col-md-4 text-center" onclick="location.href='{{route('listing',$brand['category_id'])}}';" style="cursor: pointer;">
            <div class="wg-box-content">

                {{-- <a href="{{route('listing',$brand['category_id'])}}'"> --}}

                <!-- <a href="#"> -->

                    <div class="wg-box-content-overlay"></div>
                    <img class="wg-box-content-image" src="{{ asset('images/products/'.$brand['image']) }}">
                    <div class="wg-box-content-details wg-box-fadeIn-bottom">
                        <h3>{{$brand['name']}}</h3>
                        <p>{{$brand['product_code']}}</p>
                    </div>
                <!-- </a> -->
            </div>
        </div>





<!--         <div class="col-md-4 text-center">
            <div class="content-grid-effect slow-zoom vertical">
                <div class="img-box">
                    <img src="{{ asset('images/products/'.$brand['image']) }}" alt="image" class="img-responsive zoom-img prodImg"></div>
                <div class="info-box">
                    <div class="info-content simpleCart_shelfItem">
                        <h4>{{$brand['name']}}</h4>
                        <span class="separator"></span>
                        <p><span class="item_price">{{$brand['product_code']}}</span></p>
                        <span class="separator"></span>
                        <a class="item_add hvr-outline-out button2" href="#">add to cart </a> 
                    </div>
                </div>
            </div>
        </div> -->
        @endforeach
        <div class="clearfix"></div>
    </div>
    @endif
    <!-- //content-bottom -->
    <!-- product-nav -->
    @if(!empty($homeListing))

    <div class="new_arrivals">
        <div class="container uLine" id="product">
            <h3><span>Products</span></h3>
        </div>
    </div>


<div class="row" style="margin-bottom: 5rem;">
<!--   <div class="container">

        @foreach ($homeListing as $list)
        <div class="col-md-4" onclick="location.href='{{route('listing',$list['category_id'])}}';" style="cursor: pointer;">
            <div class="product-men">
                <div class="men-pro-item simpleCart_shelfItem">
                    <div class="men-thumb-item">
                        <a href="#"><img src="{{ asset('images/products/'.$list['image']) }}" alt="" class="pro-image-front prodImg"></a>
                        <a href="#"><img src="{{ asset('images/products/'.$list['image']) }}" alt="" class="pro-image-back prodImg"></a>
                            <div class="men-cart-pro">
                                <div class="inner-men-cart-pro">
                                    <a href="{{route('listing',$list['category_id'])}}" class="link-product-add-cart">{{$list['name'] ?? ""}}  ({{ $list['product_code']??""}})</a>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
      @endforeach

  </div> -->
</div> 


<div class="row resources ">
  <div class="container" id="resource-slider">
    <button class='arrow prev'></button>
    <button class='arrow next'></button>
    <div class="resource-slider-frame">
        @foreach ($homeListing as $list)

      <div class="resource-slider-item">
        <div class="resource-slider-inset">
            <div class="product-men">
                <div class="men-pro-item simpleCart_shelfItem">
                    <div class="men-thumb-item">
                        <img src="{{ asset('images/products/'.$list['image']) }}" alt="" class="pro-image-front prodImg">
                        <img src="{{ asset('images/products/'.$list['image']) }}" alt="" class="pro-image-back prodImg">
                            <div class="men-cart-pro">
                                <div class="inner-men-cart-pro">
                                    <!-- <a href="{{route('listing',$list['category_id'])}}" class="link-product-add-cart">{{$list['name'] ?? ""}}  ({{ $list['product_code']??""}})</a> -->
                                    <span class="link-product-add-cart">{{$list['name'] ?? ""}}  ({{ $list['product_code']??""}})</span>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
      @endforeach
    </div>
  </div>
</div> 

<script type="">
            function defer(method) {
          if (window.jQuery)
            method();
          else
            setTimeout(function() {
              defer(method)
            }, 50);
        }
        defer(function() {
          (function($) {
            
            function doneResizing() {
              var totalScroll = $('.resource-slider-frame').scrollLeft();
              var itemWidth = $('.resource-slider-item').width();
              var difference = totalScroll % itemWidth;
              if ( difference !== 0 ) {
                $('.resource-slider-frame').animate({
                  scrollLeft: '-=' + difference
                }, 500, function() {
                  // check arrows
                  // checkArrows();
                });
              }
            }
            
            function checkArrows() {
              var totalWidth = $('#resource-slider .resource-slider-item').length * $('.resource-slider-item').width();
              var frameWidth = $('.resource-slider-frame').width();
              var itemWidth = $('.resource-slider-item').width();
              var totalScroll = $('.resource-slider-frame').scrollLeft();
              
              if ( ((totalWidth - frameWidth) - totalScroll) < itemWidth ) {
                $(".next").css("visibility", "hidden");
              }
              else {
                $(".next").css("visibility", "visible");
              }
              if ( totalScroll < itemWidth ) {
                $(".prev").css("visibility", "hidden");
              }
              else {
                $(".prev").css("visibility", "visible");
              }
            }
            
            $('.arrow').on('click', function() {
              var $this = $(this),
                width = $('.resource-slider-item').width(),
                speed = 500;
              if ($this.hasClass('prev')) {
                $('.resource-slider-frame').animate({
                  scrollLeft: '-=' + width
                }, speed, function() {
                  // check arrows
                  // checkArrows();
                });
              } else if ($this.hasClass('next')) {
                $('.resource-slider-frame').animate({
                  scrollLeft: '+=' + width
                }, speed, function() {
                  // check arrows
                  // checkArrows();
                });
              }
            }); // end on arrow click
            
            $(window).on("load resize", function() {
              // checkArrows();
              $('#resource-slider .resource-slider-item').each(function(i) {
                var $this = $(this),
                  left = $this.width() * i;
                $this.css({
                  left: left
                })
              }); // end each
            }); // end window resize/load
            
            var resizeId;
            $(window).resize(function() {
                clearTimeout(resizeId);
                resizeId = setTimeout(doneResizing, 500);
            });
            
          })(jQuery); // end function
        });
</script>


<a href="#" id="scroll" style="display: none;"><span></span></a>

<script type="text/javascript">
    
$(document).ready(function(){ 
    $(window).scroll(function(){ 
        if ($(this).scrollTop() > 100) { 
            $('#scroll').fadeIn(); 
        } else { 
            $('#scroll').fadeOut(); 
        } 
    }); 
    $('#scroll').click(function(){ 
        $("html, body").animate({ scrollTop: 0 }, 600); 
        return false; 
    }); 
});
</script>

<!--     <div class="product-easy">
        <div class="container">
            <div class="sap_tabs">
                <div id="horizontalTab" style="display: block; width: 100%; margin: 0px;">
                    <ul class="resp-tabs-list">
                        <li class="resp-tab-item" aria-controls="tab_item-0" role="tab"><span>Products</span></li> 
                    </ul>                    
                    <div class="resp-tabs-container">
                        <div class="tab-1 resp-tab-content" aria-labelledby="tab_item-0">
                            <div class="row">
                                @foreach ($homeListing as $list)
                                    <div class="col-md-4 product-men">
                                        <div class="men-pro-item simpleCart_shelfItem">
                                            <div class="men-thumb-item">
!--                                                 <img src="{{ asset('images/products/'.$list['image']) }}" alt="" class="pro-image-front prodImg">
                                                <img src="{{ asset('images/products/'.$list['image']) }}" alt="" class="pro-image-back prodImg"> --
                                                <img src="{{ asset('images/products/'.$brand['image']) }}" alt="" class="pro-image-front prodImg">
                                                <img src="{{ asset('images/products/'.$brand['image']) }}" alt="" class="pro-image-back prodImg">
                                                    <div class="men-cart-pro">
                                                        <div class="inner-men-cart-pro">
                                                            <a href="{{route('listing',$list['category_id'])}}" class="link-product-add-cart">{{$list['name'] ?? ""}}  ({{ $list['product_code']??""}})</a>
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="clearfix"></div> 

                        </div>
                    </div>  
                </div>
            </div>
        </div>
    </div> -->
    @endif
    <!-- //product-nav -->
    @include('webinc.catfooter')
@endsection



