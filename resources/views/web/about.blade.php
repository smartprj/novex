@extends('layouts.web')
@section('content')
<div class="page-head">
    <div class="container">
        <h3 class="pageHeadTitle">About Us<br><b><span class="pageSubHeadTitle">RSONS PLUMBING SOLUTION PRIVATE LIMITED</span></b></h3>
    </div>
</div>
<div class="new_arrivals">

    <div class="row" style="margin-top: 5rem;">
        <div class="col-md-6">
            <div class="uLine">
                <h3><span>About Us</span></h3>
            </div>
            <p><b>Rsons Plumbing Solution Pvt Ltd</b> is a leading manufacturer Wholesaler and supplier of
            Bathroom Fittings and Bath Accessories in stainless steel, Brass and ABS under the brand name
            <b>NOVEX</b> and <b>COSFLO</b>. Is located in India. Our Products are supported by a professional
            designing team.</p>
            <br><br>
            <p>
                <b>“We Believes in Producing Superior Quality Bath Fittings”</b>
            </p>         
        </div>
        <div class="col-md-6" style="text-align: center;">
            <img src="{{ asset('web/images/aboutus2.jpg') }}" class="aboutus_img img-responsive" />
        </div>
    </div>

    <div class="row" style="margin-top: 5rem; margin-bottom:3rem;">
        <div class="col-md-6" style="text-align: center;">
             <img src="{{ asset('web/images/aboutus1.jpg') }}" class="aboutus_img img-responsive" />
        </div>
        <div class="col-md-6">
            <div class="uLine">
                <h3><span>Design Quality</span></h3>
            </div>

            <p>
                Rsons Plumbing Solution Pvt Ltd is committed to excellent quality, design and innovation. For
                that we applied modern technology and skilled manpower to produce and excellent product.
                
            </p>
            <br>
            <p>
                At NOVEX, we believe that quality is a paramount in order to create value and achieve customer satisfaction. Our State-of-the-art integrated manufacturing facilities, integrated with highly advanced technology for making world class products.
            </p>
        </div>
    </div>
<!--     <div class="container">
        <p>Established in the year 2014, we, RSONS PLUMBING SOLUTION PRIVATE LIMITED, is leading Importers, manufacturer, Wholesalers and suppliers of CP Bathroom Fittings, SS fitting and Bath Accessories   under the brand name NOVEX. Our State-of-the-art integrated production facilities located in India’ our head office in Chawri Bazaar, Delhi.  Are well supported by a professional designing team with professional employee.
        “We Believes in Producing Superior Quality Bath Fittings”
        Our factory has set up a sophisticated and modern infrastructure and we have Manufacturing units that help us in offering bulk order in time. Apart from these, we have a large and capacious warehouse that keep and store all the finished products in a Systematic and organized manner.</p>
        <br>
        <h3><span>DESIGN QUALITY</span></h3>
        <p>Rsons Plumbing Solution Pvt Ltd is committed to excellent quality, design and innovation. For that we applied modern technology and skilled manpower to produce and excellent product.
        At NOVEX, we believe that quality is a paramount in order to create value and achieve customer satisfaction. Our State-of-the-art integrated manufacturing facilities, integrated with highly advanced technology for making world class products.</p>
        
    </div> -->
</div>
@endsection