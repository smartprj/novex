<nav class="sidebar sidebar-offcanvas" id="sidebar">
  <ul class="nav">



    <li class="nav-item">
      <a class="nav-link" href="{{route('home')}}">
        <span class="menu-title">Dashboard</span>
        <i class="mdi mdi-home menu-icon"></i>
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{route('categories.index')}}">
        <span class="menu-title">Categories</span>
        <i class="mdi mdi-crosshairs-gps menu-icon"></i>
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{route('products.index')}}">
        <span class="menu-title">Products</span>
        <i class="mdi mdi-medical-bag menu-icon"></i>
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{route('contactlist')}}">
        <span class="menu-title">Contact Us List</span>
        <i class="mdi mdi-medical-bag menu-icon"></i>
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{route('enquiryform')}}">
        <span class="menu-title">Enquiry Form List</span>
        <i class="mdi mdi-medical-bag menu-icon"></i>
      </a>
    </li>


    <!-- <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#ui-category" aria-expanded="false" aria-controls="ui-category">
        <span class="menu-title">Category</span>
        <i class="menu-arrow"></i>
        <i class="mdi mdi-crosshairs-gps menu-icon"></i>
      </a>
      <div class="collapse" id="ui-category">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"> <a class="nav-link" href="{{ route('categories.index') }}">Listing</a></li>
          <li class="nav-item"> <a class="nav-link" href="{{ route('categories.create') }}">Add category</a></li>
        </ul>
      </div>
    </li>

    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#ui-products" aria-expanded="false" aria-controls="ui-products">
        <span class="menu-title">Products</span>
        <i class="menu-arrow"></i>
        <i class="mdi mdi-medical-bag menu-icon"></i>
      </a>
      <div class="collapse" id="ui-products">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"> <a class="nav-link" href="{{ route('products.index') }}"> Listing </a></li>
          <li class="nav-item"> <a class="nav-link" href="{{ route('products.create') }}"> Add Product </a></li>
        </ul>
      </div>
    </li> -->

    <!-- <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#ui-pages" aria-expanded="false" aria-controls="ui-pages">
        <span class="menu-title">Pages</span>
        <i class="menu-arrow"></i>
        <i class="mdi mdi-format-list-bulleted menu-icon"></i>
      </a>
      <div class="collapse" id="ui-pages">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"> <a class="nav-link" href="{{ route('pages.index') }}">Listing</a></li>
          <li class="nav-item"> <a class="nav-link" href="{{ route('pages.index') }}">Add Pages</a></li>
        </ul>
      </div>
    </li> -->

  </ul>
</nav>