<?php
use Illuminate\Support\Facades\Route;

// Route::get('/', function () {
//     return view('layouts.web');
// });
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:cache');
    $exitCode = Artisan::call('config:clear');
    return '<h1>Cache facade value cleared</h1>';
    // return what you want
});

Route::any('/','WebController@index')->name('web');
Route::any('/about-us','WebController@about')->name('aboutus');
Route::any('/contact-us','WebController@contactus')->name('contactus');
Route::any('/enquiry','WebController@enquiry')->name('enquiry');
Route::any('/storecontactus','WebController@storecontactus')->name('storecontactus');
Route::any('/storeenquiry','WebController@storeenquiry')->name('storeenquiry');
Route::any('/product/{id}','WebController@listing')->name('listing');
Route::any('/detail/{id}','WebController@detail')->name('detail');

Route::group(['prefix' => 'admin'],function () {
	Auth::routes();
   	Route::get('/home', 'HomeController@index')->name('home');
	Route::resource('categories','CategoryController');
	Route::resource('products','ProductController');
	Route::resource('pages','PageController');
	Route::get('subcat','CategoryController@subcat')->name('subcat');
	Route::any('contact-us','HomeController@contactUs')->name('contactlist');
	Route::any('enquiry','HomeController@enquiry')->name('enquiryform');
	Route::any('changelistingstatus','ProductController@changelistingstatus')->name('changelistingstatus');
	Route::any('changebrandingstatus','ProductController@changebrandingstatus')->name('changebrandingstatus');
	Route::any('categorylistingstatus','CategoryController@categorylistingstatus')->name('categorylistingstatus');
});

