<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->insert([
    		'name'=>'admin',
    		'username'=>'admin',
    		'email'=>'admin@novex.com',
    		'password'=>bcrypt('admin@123')
    	]);
    }
}
