<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Contactus;
use App\Enquiry;

class HomeController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
        $this->contactus = new Contactus;
        $this->enquiry = new Enquiry;
    }

    public function index()
    {
        return view('home.dashboard');
    }

    public function contactUs()
    {
    	$contacts = $this->contactus->latest()->paginate(5);
    	return view('home.contactus',compact('contacts'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function enquiry()
    {
        $enquiries = $this->enquiry->latest()->paginate(5);
        return view('home.enquiry',compact('enquiries'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
}
