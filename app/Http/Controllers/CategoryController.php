<?php
namespace App\Http\Controllers;
use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->category = new Category;
    }

    public function index()
    {
        $catListing = $this->category->distinct('name')->pluck('name','id');
        // $categories = $this->category->search($where)->latest()->paginate(10);
        $categories = Category::latest()->paginate(5);
        return view('categories.index',compact('categories','catListing'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function create()
    {
        $catListing = $this->category->distinct('name')->pluck('name','id');
        return view('categories.create',compact('catListing'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'status' => 'required',
        ]);
        Category::create($request->all());
        return redirect()->route('categories.index')
                        ->with('success','Category created successfully.');
    }

    public function show(Category $category)
    {
        return view('categories.show',compact('category'));
    }

    public function edit(Category $category)
    {
        $catListing = $this->category->distinct('name')->pluck('name','id');
        return view('categories.edit',compact('category','catListing'));
    }

    public function update(Request $request, Category $category)
    {
        $request->validate([
            'name' => 'required',
            'status' => 'required',
        ]);
        $category->update($request->all());
        return redirect()->route('categories.index')
                        ->with('success','Category updated successfully');
    }

    public function categorylistingstatus(Request $request){
        if(!empty($request->id)){
            $chkStatus = $this->category->where('id',$request->id)->first();
            if($chkStatus['home_dispaly'] == 1){
                $data['home_dispaly'] = 0;
            }else{
                $data['home_dispaly'] = 1;
            }
            $this->category->where('id',$request->id)->update($data);
        }
        return true;
    }

    public function destroy(Category $category)
    {
        $category->delete();
        return redirect()->route('categories.index')
                        ->with('success','Category deleted successfully');
    }

    public function subcat(Request $request){
        $category = [];
        if(!empty($request->id)){
            $category = $this->category->where('parent_id',$request->id)->pluck('name','id')->toArray();
        }
        return view('categories.subcat',compact('category'));
    }
}
