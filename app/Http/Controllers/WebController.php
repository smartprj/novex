<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Category;
use App\Product;
use App\Contactus;
use App\Enquiry;
use Mail;

class WebController extends Controller
{

	public function __construct(){
		$this->category     = new Category;
        $this->product      = new Product;
        $this->contactus    = new Contactus;
        $this->enquiry      = new Enquiry;
	}

    public function index(){
    	$categorylist      = $this->getCategories();
        $homeListing       = $this->product->where(['home_listing'=>1])->get()->toArray();
        $brandListing      = $this->product->where(['home_branding'=>1])->get()->toArray();
        $catHomeListing    = $this->category->WhereNull('parent_id')->where(['home_dispaly'=>1])->get()->toArray();
    	return view('web.home',compact('categorylist','homeListing','brandListing','catHomeListing'));
    }

    public function about(){
        $categorylist = $this->getCategories();
        return view('web.about',compact('categorylist'));
    }

    public function listing($slug){
        $categorylist   = $this->getCategories();
        $getCatDetail   = $this->category->where(['id'=>$slug])->get()->first();
        if(!empty($getCatDetail)){
            $getCatDetail = $getCatDetail->toArray();
        }
        // if(!empty($getCatDetail['parent_id'])){
        //     $where['sub_category_id'] = $slug;
        // }else{
        //     $where['category_id'] = $slug;
        // }
        $products       = $this->product->where(['status'=>1])->where(function ($products) use ($slug) {
                        $products->where(['category_id'=>$slug])->orWhere(['sub_category_id'=>$slug]);
                        })->get()->toArray();
        return view('web.listing',compact('categorylist','products','getCatDetail'));
    }

    public function detail($product_code){
        $categorylist   = $this->getCategories();
        $product        = $this->product->where(['product_code'=>$product_code])->get()->first()->toArray();
        $catListing     = $this->category->pluck('name','id');
        return view('web.detail',compact('categorylist','product','catListing'));
    }

    public function storecontactus(Request $request){
        $data               = $request->all();
        $this->contactus->create($data);
        $template           = "mail.contactus";
        $data['subject']    = " Contact Us Request ";
        $data['desc']       = $data['message'];
        $this->send($data,$template);
        return redirect()->route('contactus')
                        ->with('success','Contact Details Added Successfully.');
    }

    public function storeenquiry(Request $request){
        $data               = $request->all();
        $this->enquiry->create($data);
        $template           = "mail.enquiry";
        $data['subject']    = " Enquiry Form Request ";
        $data['desc']       = $data['message'];
        $this->send($data,$template);
        return redirect()->route('enquiry')
                        ->with('success','Enquiry Form Submitted Successfully.');
    }

    public function contactus(){
        $categorylist = $this->getCategories();
        $catHomeListing    = $this->category->WhereNull('parent_id')->where(['home_dispaly'=>1])->get()->toArray();
        return view('web.contactus',compact('categorylist','catHomeListing'));
    }

    public function enquiry(){
        $categorylist = $this->getCategories();
        $catHomeListing    = $this->category->WhereNull('parent_id')->where(['home_dispaly'=>1])->get()->toArray();
        return view('web.enquiry',compact('categorylist','catHomeListing'));
    }


    private function getCategories($parentid=''){
    	$arr=[];
    	$subArr=[];
    	if(empty($parentid)){
    		$catListing = $this->category->WhereNull('parent_id')->pluck('name','id')->toArray();
    	}else{
    		$catListing = $this->category->Where('parent_id',$parentid)->pluck('name','id')->toArray();
    	}
    	foreach ($catListing as $key => $value) {
			$arr[$key]['parent']=$value;
			$subArr = $this->getCategories($key);	
			if(!empty($subArr)){
				$arr[$key]['child']=[];
				$arr[$key]['child']=$subArr;
			}
    	}
    	return $arr;
    }

    public function send($data,$template){
        Mail::send(
          ['html' => $template],
          ['data' => $data],
          function($message) use ($data) {
              $message->to('info@rsonsplumbing.com','Rsons Plumbing');
              if(!empty($data['subject'])){
                $message->subject($data['subject']);
              }
              $message->from($data['email'],$data['name']);
          }
       );
    }
}
