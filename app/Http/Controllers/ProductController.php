<?php
namespace App\Http\Controllers;
use App\Category;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
        $this->category = new Category;
        $this->product  = new Product;
    }

    public function index()
    {
        $catListing = $this->category->pluck('name','id');
        // $products = $this->product->search($where)->latest()->paginate(10);
        $products = $this->product->latest()->paginate(5);
        return view('products.index',compact('products','catListing'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function create()
    {
        $catListing = $this->category->distinct('name')->pluck('name','id');
        return view('products.create',compact('catListing'));
    }

    public function store(Request $request)
    {
        $this->productValidate($request);
        $data           = $request->all();
        $data['image']  = $this->product->uploadProductImages($data['product_image'],'product');
        $this->product->create($data);
        return redirect()->route('products.index')
                        ->with('success','Product created successfully.');
    }

    public function show(Product $product)
    {
        $catListing = $this->category->pluck('name','id');
        return view('products.show',compact('product','catListing'));
    }

    public function edit(Product $product)
    {
        $catListing     = $this->category->distinct('name')->pluck('name','id');
        $subCatListing  = $this->category->where(['parent_id'=>$product->category_id])->pluck('name','id');
        return view('products.edit',compact('product','catListing','subCatListing'));
    }

    public function update(Request $request, Product $product)
    {
        $this->productValidate($request,$product->id);
        $data               = $request->all();
        if(!empty($data['product_image'])){
            $data['image']  = $this->product->uploadProductImages($data['product_image'],'product');
        }
        $product->update($data);
        return redirect()->route('products.index')
                        ->with('success','Product updated successfully');
    }

    public function destroy(Product $product)
    {
        $product->delete();
        return redirect()->route('products.index')
                        ->with('success','Product deleted successfully');
    }

    public function changelistingstatus(Request $request){
        if(!empty($request->id)){
            $chkStatus = $this->product->where('id',$request->id)->first();
            if($chkStatus['home_listing'] == 1){
                $data['home_listing'] = 0;
            }else{
                $data['home_listing'] = 1;
            }
            $this->product->where('id',$request->id)->update($data);
        }
        return true;
    }

    public function changebrandingstatus(Request $request){
        if(!empty($request->id)){
            $chkStatus = $this->product->where('id',$request->id)->first();
            if($chkStatus['home_branding'] == 1){
                $data['home_branding'] = 0;
            }else{
                $data['home_branding'] = 1;
            }
            $this->product->where('id',$request->id)->update($data);
        }
        return true;
    }

    private function productValidate($request,$id=null){
        $validate['name']                       = 'required';
        $validate['status']                     = 'required';
        $validate['category_id']                = 'required';
        $validate['product_image']              = 'max:2000';
        if(!empty($id)){
            $validate['product_code']           = 'required|unique:products,product_code,'.$id.',id';
        }else{
            $validate['product_code']           = 'required|unique:products,product_code,NULL,id';
            $validate['product_image']          = 'required';
        }

        $messages = [
           'category_id.required'       => __('Please Select Category'),
           'product_image.required'     => __('Please Select Image'),
           'product_image.max'          => __('Please Upload Image Less Than 2MB'),
           'status.required'            => __('Please Select Status'),
           'name.required'              => __('Please Enter Product Name'),
           'product_code.required'      => __('Please Enter Product Code'),
           'product_code.unique'        => __('Please Enter Unique Product Code')
        ];
        $request->validate($validate,$messages);
    }
}
