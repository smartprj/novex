<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Enquiry extends Model
{
    protected $table        = 'enquiry_form';
    protected $primaryKey   = 'id';
    protected $fillable     = [
        'name','email', 'number','url','city','message','created_at','updated_at'
    ];

    public static function scopeSearch($query,$where=[]){
        return $query->where($where);
    }
}
