<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table        = 'categories';
    protected $primaryKey   = 'id';
    protected $fillable     = [
        'parent_id', 'name','status','home_dispaly', 'created_at', 'created_by', 'updated_at', 'updated_by'
    ];

    public static function scopeSearch($query,$where=[]){
        return $query->where($where);
    }
}
