<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Contactus extends Model
{
    protected $table        = 'contactus';
    protected $primaryKey   = 'id';
    protected $fillable     = [
        'name','email', 'number','url','city','message','created_at','updated_at'
    ];

    public static function scopeSearch($query,$where=[]){
        return $query->where($where);
    }
}
