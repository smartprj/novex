<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table        = 'products';
    protected $primaryKey   = 'id';
    protected $fillable     = [
        'category_id','sub_category_id', 'name','product_code','description','image','status','home_listing','home_branding', 'created_at', 'created_by', 'updated_at', 'updated_by'
    ];

    public static function scopeSearch($query,$where=[]){
        return $query->where($where);
    }

    public static function uploadProductImages($image,$name) {
        $imageName = $name.time().'.'.$image->extension();
        $image->move(public_path('images/products/'), $imageName);
        $imageName = filter_var($imageName,FILTER_SANITIZE_STRING);
        return $imageName;
    }
}
